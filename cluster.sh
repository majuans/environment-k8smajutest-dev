export PROJECT_ID=k8smajutest
gcloud container get-server-config

export VERSION=1.15.12-gke.3 # Replace [...] with k8s version. Make sure that it is v1.14+.

gcloud container clusters \
    create $PROJECT_ID \
    --project $PROJECT_ID \
    --region us-east1 \
    --machine-type n1-standard-4 \
    --cluster-version=$VERSION \
    --num-nodes=1 \
    --enable-autorepair \
    --enable-autoupgrade     

kubectl create clusterrolebinding \
    cluster-admin-binding \
    --clusterrole cluster-admin \
    --user $(gcloud config get-value account)